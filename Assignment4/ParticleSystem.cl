

float4 cross3(float4 a, float4 b){
	float4 c;
	c.x = a.y * b.z - b.y * a.z;
	c.y = a.z * b.x - b.z * a.x;
	c.z = a.x * b.y - b.x * a.y;
	c.w = 0.f;
	return c;
}

float dot3(float4 a, float4 b){
	return a.x*b.x + a.y*b.y + a.z*b.z;
}


#define EPSILON 0.001F

// This function expects two points defining a ray (x0 and x1)
// and three vertices stored in v1, v2, and v3 (the last component is not used)
// it returns true if an intersection is found and sets the isectT and isectN
// with the intersection ray parameter and the normal at the intersection point.
bool LineTriangleIntersection(	float4 x0, float4 x1,
								float4 v1, float4 v2, float4 v3,
								float *isectT, float4 *isectN){

	float4 dir = x1 - x0;
	dir.w = 0.f;

	float4 e1 = v2 - v1;
	float4 e2 = v3 - v1;
	e1.w = 0.f;
	e2.w = 0.f;

	float4 s1 = cross3(dir, e2);
	float divisor = dot3(s1, e1);
	if (divisor == 0.f)
		return false;
	float invDivisor = 1.f / divisor;

	// Compute first barycentric coordinate
	float4 d = x0 - v1;
	float b1 = dot3(d, s1) * invDivisor;
	if (b1 < -EPSILON || b1 > 1.f + EPSILON)
		return false;

	// Compute second barycentric coordinate
	float4 s2 = cross3(d, e1);
	float b2 = dot3(dir, s2) * invDivisor;
	if (b2 < -EPSILON || b1 + b2 > 1.f + EPSILON)
		return false;

	// Compute _t_ to intersection point
	float t = dot3(e2, s2) * invDivisor;
	if (t < -EPSILON || t > 1.f + EPSILON)
		return false;

	// Store the closest found intersection so far
	*isectT = t;
	*isectN = cross3(e1, e2);
	*isectN = normalize(*isectN);
	return true;

}


bool CheckCollisions(	float4 x0, float4 x1,
						__global float4 *gTriangleSoup, 
						__local float4* lTriangleCache,	// The cache should hold as many vertices as the number of threads (therefore the number of triangles is nThreads/3)
						uint nTriangles,
						float  *t,
						float4 *n){

	bool found = false;
	uint lid = get_local_id(0);
	uint k = get_local_size(0) / 3;

	uint nProcessed = 0;
	while (nProcessed < nTriangles)
	{
		barrier(CLK_LOCAL_MEM_FENCE);
		lTriangleCache[lid] = gTriangleSoup[3 * nProcessed + lid];
		barrier(CLK_LOCAL_MEM_FENCE);

		uint m = min(nTriangles - nProcessed, k);
		for (uint i = 0; i < m; i++)
		{
			float isectT;
			float4 isectN;
			if (!LineTriangleIntersection(x0, x1, lTriangleCache[3 * i], lTriangleCache[3 * i + 1], lTriangleCache[3 * i + 2], &isectT, &isectN))
				continue;

			if (!found || isectT < *t)
			{
				*t = isectT;
				*n = isectN;
			}
			found = true;
		}

		nProcessed += k;
	}

	return found;
}
								

float4 reflect(float4 d, float4 n)
{
	return d - 2.0F * dot3(d, n) * n;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This is the integration kernel. Implement the missing functionality
//
// Input data:
// gAlive         - Field of flag indicating whether the particle with that index is alive (!= 0) or dead (0). You will have to modify this
// gForceField    - 3D texture with the  force field
// sampler        - 3D texture sampler for the force field (see usage below)
// nParticles     - Number of input particles
// nTriangles     - Number of triangles in the scene (for collision detection)
// lTriangleCache - Local memory cache to be used during collision detection for the triangles
// gTriangleSoup  - The triangles in the scene (layout see the description of CheckCollisions())
// gPosLife       - Position (xyz) and remaining lifetime (w) of a particle
// gVelMass       - Velocity vector (xyz) and the mass (w) of a particle
// dT             - The timestep for the integration (the has to be subtracted from the remaining lifetime of each particle)
//
// Output data:
// gAlive   - Updated alive flags
// gPosLife - Updated position and lifetime
// gVelMass - Updated position and mass
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__kernel void Integrate(__global uint *gAlive,
						__read_only image3d_t gForceField, 
						sampler_t sampler,
						uint nParticles,
						uint nTriangles,
						__local float4 *lTriangleCache,
						__global float4 *gTriangleSoup,
						__global float4 *gPosLife, 
						__global float4 *gVelMass,
						float dT
						)  {

	float4 gAccel = (float4)(0.F, -9.81F, 0.F, 0.F);
	uint gid = get_global_id(0);

	// Verlet Velocity Integration
	float4 x0 = gPosLife[gid];
	float4 v0 = gVelMass[gid];

	float mass = v0.w;
	float life = x0.w - dT;

	float4 F0 = read_imagef(gForceField, sampler, x0);
	float4 a0 = F0 / mass + gAccel;

	float4 xT = x0 + v0 * dT + a0 * dT * dT / 2;
	float4 FT = read_imagef(gForceField, sampler, xT);
	float4 aT = FT / mass + gAccel;

	float4 vT = v0 + (a0 + aT) * dT / 2;
	
	// Check for collisions and correct the position and velocity of the particle if it collides with a triangle
	// - Don't forget to offset the particles from the surface a little bit, otherwise they might get stuck in it.
	// - Dampen the velocity (e.g. by a factor of 0.7) to simulate dissipation of the energy.
	float t;
	float4 n;
	if (CheckCollisions(x0, xT, gTriangleSoup, lTriangleCache, nTriangles, &t, &n))
	{
		xT = x0 + (xT - x0) * (t - EPSILON);
		xT += n * 0.1F;
		vT = reflect(0.7F * vT, n);
	}

	// Kill the particle if its life is < 0.0 by setting the corresponding flag in gAlive to 0.
	gAlive[gid] = life < 0 ? 0 : 1;

	// Independently of the status of the particle, possibly create a new one.
	// For instance, if the particle gets too fast (or too high, or passes through some region), it is split into two...
	uint newId = gid + nParticles;
	if (life < 10.F)
	{
		float4 xN = xT;
		xN.w = 100.F;

		float4 vN = vT;
		vN.w = mass;

		gPosLife[newId] = xN;
		gVelMass[newId] = vN;
		gAlive[newId] = 1;
	}

	xT.w = life;
	vT.w = mass;

	gPosLife[gid] = xT;
	gVelMass[gid] = vT;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__kernel void Clear(__global float4* gPosLife, __global float4* gVelMass) {
	uint GID = get_global_id(0);
	gPosLife[GID] = 0.f;
	gVelMass[GID] = 0.f;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__kernel void Reorganize(	__global uint* gAlive, __global uint* gRank,	
							__global float4* gPosLifeIn,  __global float4* gVelMassIn,
							__global float4* gPosLifeOut, __global float4* gVelMassOut) {


	// Re-order the particles according to the gRank obtained from the parallel prefix sum
	uint gid = get_global_id(0);
	uint rank = gRank[gid];
	uint gs = get_global_size(0);

	if (rank >= gs / 2)
		return;

	if ((gid >= gs - 2 && rank > 0) || rank < gRank[gid + 1])
	{
		gAlive[rank] = 1;
		gPosLifeOut[rank] = gPosLifeIn[gid];
		gVelMassOut[rank] = gVelMassIn[gid];
	}
}
