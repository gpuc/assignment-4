#define DAMPING 0.02f

#define G_ACCEL (float4)(0.f, -9.81f, 0.f, 0.f)

#define WEIGHT_ORTHO	0.138f
#define WEIGHT_DIAG		0.097f
#define WEIGHT_ORTHO_2	0.069f
#define WEIGHT_DIAG_2	0.048f


#define ROOT_OF_2 1.4142135f
#define DOUBLE_ROOT_OF_2 2.8284271f


float4 wind(float simulationTime)
{
	float4 direction = (float4)(0, 0, 0.5f, 0);
	return direction * sin(simulationTime);
}

///////////////////////////////////////////////////////////////////////////////
// The integration kernel
// Input data:
// width and height - the dimensions of the particle grid
// d_pos - the most recent position of the cloth particle while...
// d_prevPos - ...contains the position from the previous iteration.
// elapsedTime      - contains the elapsed time since the previous invocation of the kernel,
// prevElapsedTime  - contains the previous time step.
// simulationTime   - contains the time elapsed since the start of the simulation (useful for wind)
// All time values are given in seconds.
//
// Output data:
// d_prevPos - Input data from d_pos must be copied to this array
// d_pos     - Updated positions
///////////////////////////////////////////////////////////////////////////////
  __kernel void Integrate(unsigned int width,
						unsigned int height, 
						__global float4* d_pos,
						__global float4* d_prevPos,
						float elapsedTime,
						float prevElapsedTime,
						float simulationTime) {
							
	// Make sure the work-item does not map outside the cloth
    if (get_global_id(0) >= width || get_global_id(1) >= height)
		return;

	unsigned int particleID = get_global_id(0) + get_global_id(1) * width;
	// This is just to keep every 8th particle of the first row attached to the bar
    if (particleID > width - 1 || (particleID & (7)) != 0) {

		// Read the positions
		float4 x = d_pos[particleID];
		float4 xPrev = d_prevPos[particleID];
		float4 v = (prevElapsedTime == 0) ? 0 : ((x - xPrev) / prevElapsedTime);
		
		// Compute the new one position using the Verlet position integration, taking into account gravity and wind
		float4 a = G_ACCEL + wind(simulationTime);
		float4 xNext = x + v * elapsedTime + a * elapsedTime * elapsedTime / 2;

		// Move the value from d_pos into d_prevPos and store the new one in d_pos
		d_prevPos[particleID] = x;
		d_pos[particleID] = xNext;
	}
}



///////////////////////////////////////////////////////////////////////////////
// Input data:
// pos1 and pos2 - The positions of two particles
// restDistance  - the distance between the given particles at rest
//
// Return data:
// correction vector for particle 1
///////////////////////////////////////////////////////////////////////////////
  float4 SatisfyConstraint(float4 pos1,
						 float4 pos2,
						 float restDistance){
	if (any(isnan(pos2)))
		return 0;

	float4 toNeighbor = pos2 - pos1;
	return (toNeighbor - normalize(toNeighbor) * restDistance);
}

///////////////////////////////////////////////////////////////////////////////
// Input data:
// width and height - the dimensions of the particle grid
// restDistance     - the distance between two orthogonally neighboring particles at rest
// d_posIn          - the input positions
//
// Output data:
// d_posOut - new positions must be written here
///////////////////////////////////////////////////////////////////////////////

#define TILE_X 16 
#define TILE_Y 16
#define HALOSIZE 2

#define INDEX(x, y) ((y) * width + (x))
#define CHECK_BOUNDS(i, s) ((i) >= 0 && (i) < (s))
#define DATA(x, y) (CHECK_BOUNDS(x, width) && CHECK_BOUNDS(y, height) ? d_posIn[INDEX(x, y)] : NAN)


__kernel __attribute__((reqd_work_group_size(TILE_X, TILE_Y, 1)))
__kernel void SatisfyConstraints(unsigned int width,
								unsigned int height, 
								float restDistance,
								__global float4* d_posOut,
								__global float4 const * d_posIn){
    
	uint2 gid;
	gid.x = get_global_id(0);
	gid.y = get_global_id(1);

	unsigned int particleID = gid.x + gid.y * width;

	uint2 lid;
	lid.x = get_local_id(0);
	lid.y = get_local_id(1);

	uint2 tid;
	tid.x = lid.x + HALOSIZE;
	tid.y = lid.y + HALOSIZE;

	__local float4 tile[TILE_Y + 2 * HALOSIZE][TILE_X + 2 * HALOSIZE];

	// load tile
	tile[tid.y][tid.x] = DATA(gid.x, gid.y);

	// load halo
	if (lid.y == 0) // top row
	{
		tile[0][tid.x] = DATA(gid.x, gid.y - 2);
	}
	else if (lid.y == 1) // second row
	{
		tile[1][tid.x] = DATA(gid.x, gid.y - 2);
	}
	else if (lid.y == TILE_Y - 2) // second to last row
	{
		tile[TILE_Y + 2 * HALOSIZE - 2][tid.x] = DATA(gid.x, gid.y + 2);
	}
	else if (lid.y == TILE_Y - 1) // last row
	{
		tile[TILE_Y + 2 * HALOSIZE - 1][tid.x] = DATA(gid.x, gid.y + 2);
	}

	if (lid.x == 0) // first column
	{
		tile[tid.y][0] = DATA(gid.x - 2, gid.y);
	}
	else if (lid.x == 1) // second column
	{
		tile[tid.y][1] = DATA(gid.x - 2, gid.y);
	}
	else if (lid.x == TILE_X - 2) // second to last column
	{
		tile[tid.y][TILE_X + 2 * HALOSIZE - 2] = DATA(gid.x + 2, gid.y);
	}
	else if (lid.x == TILE_X - 1) // last column
	{
		tile[tid.y][TILE_X + 2 * HALOSIZE - 1] = DATA(gid.x + 2, gid.y);
	}

	if (lid.y == 0 && lid.x == 0) // top left corner
	{
		tile[0][0] = DATA(gid.x - 2, gid.y - 2);
		tile[0][1] = DATA(gid.x - 1, gid.y - 2);
		tile[1][0] = DATA(gid.x - 2, gid.y - 1);
		tile[1][1] = DATA(gid.x - 1, gid.y - 1);
	}
	else if (lid.y == TILE_Y - 1 && lid.x == 0) // bottom left corner
	{
		tile[TILE_Y + 2 * HALOSIZE - 2][0] = DATA(gid.x - 2, gid.y + 1);
		tile[TILE_Y + 2 * HALOSIZE - 2][1] = DATA(gid.x - 1, gid.y + 1);
		tile[TILE_Y + 2 * HALOSIZE - 1][0] = DATA(gid.x - 2, gid.y + 2);
		tile[TILE_Y + 2 * HALOSIZE - 1][1] = DATA(gid.x - 1, gid.y + 2);
	}
	else if (lid.y == 0 && lid.x == TILE_X - 1) // top right corner
	{
		tile[0][TILE_X + 2 * HALOSIZE - 2] = DATA(gid.x + 1, gid.y - 2);
		tile[0][TILE_X + 2 * HALOSIZE - 1] = DATA(gid.x + 2, gid.y - 2);
		tile[1][TILE_X + 2 * HALOSIZE - 2] = DATA(gid.x + 1, gid.y - 1);
		tile[1][TILE_X + 2 * HALOSIZE - 1] = DATA(gid.x + 2, gid.y - 1);
	}
	else if (lid.y == TILE_Y - 1 && lid.x == TILE_X - 1) // bottom right corner
	{
		tile[TILE_Y + 2 * HALOSIZE - 2][TILE_X + 2 * HALOSIZE - 2] = DATA(gid.x + 1, gid.y + 1);
		tile[TILE_Y + 2 * HALOSIZE - 2][TILE_X + 2 * HALOSIZE - 1] = DATA(gid.x + 2, gid.y + 1);
		tile[TILE_Y + 2 * HALOSIZE - 1][TILE_X + 2 * HALOSIZE - 2] = DATA(gid.x + 1, gid.y + 2);
		tile[TILE_Y + 2 * HALOSIZE - 1][TILE_X + 2 * HALOSIZE - 1] = DATA(gid.x + 2, gid.y + 2);
	}

	barrier(CLK_LOCAL_MEM_FENCE);

	if (gid.x >= width || gid.y >= height)
		return;

	float4 result = 0;

	// This is just to keep every 8th particle of the first row attached to the bar
	if (particleID > width - 1 || (particleID & (7)) != 0) {
		// structural constraints
		result += SatisfyConstraint(tile[tid.y][tid.x], tile[tid.y][tid.x + 1], restDistance) * WEIGHT_ORTHO;
		result += SatisfyConstraint(tile[tid.y][tid.x], tile[tid.y][tid.x - 1], restDistance) * WEIGHT_ORTHO;
		result += SatisfyConstraint(tile[tid.y][tid.x], tile[tid.y + 1][tid.x], restDistance) * WEIGHT_ORTHO;
		result += SatisfyConstraint(tile[tid.y][tid.x], tile[tid.y - 1][tid.x], restDistance) * WEIGHT_ORTHO;

		result += SatisfyConstraint(tile[tid.y][tid.x], tile[tid.y][tid.x + 2], restDistance * 2) * WEIGHT_ORTHO_2;
		result += SatisfyConstraint(tile[tid.y][tid.x], tile[tid.y][tid.x - 2], restDistance * 2) * WEIGHT_ORTHO_2;
		result += SatisfyConstraint(tile[tid.y][tid.x], tile[tid.y + 2][tid.x], restDistance * 2) * WEIGHT_ORTHO_2;
		result += SatisfyConstraint(tile[tid.y][tid.x], tile[tid.y - 2][tid.x], restDistance * 2) * WEIGHT_ORTHO_2;

		// shear constraints
		result += SatisfyConstraint(tile[tid.y][tid.x], tile[tid.y + 1][tid.x + 1], restDistance * ROOT_OF_2) * WEIGHT_DIAG;
		result += SatisfyConstraint(tile[tid.y][tid.x], tile[tid.y + 1][tid.x - 1], restDistance * ROOT_OF_2) * WEIGHT_DIAG;
		result += SatisfyConstraint(tile[tid.y][tid.x], tile[tid.y - 1][tid.x + 1], restDistance * ROOT_OF_2) * WEIGHT_DIAG;
		result += SatisfyConstraint(tile[tid.y][tid.x], tile[tid.y - 1][tid.x - 1], restDistance * ROOT_OF_2) * WEIGHT_DIAG;

		result += SatisfyConstraint(tile[tid.y][tid.x], tile[tid.y + 2][tid.x + 2], restDistance * DOUBLE_ROOT_OF_2) * WEIGHT_DIAG_2;
		result += SatisfyConstraint(tile[tid.y][tid.x], tile[tid.y + 2][tid.x - 2], restDistance * DOUBLE_ROOT_OF_2) * WEIGHT_DIAG_2;
		result += SatisfyConstraint(tile[tid.y][tid.x], tile[tid.y - 2][tid.x + 2], restDistance * DOUBLE_ROOT_OF_2) * WEIGHT_DIAG_2;
		result += SatisfyConstraint(tile[tid.y][tid.x], tile[tid.y - 2][tid.x - 2], restDistance * DOUBLE_ROOT_OF_2) * WEIGHT_DIAG_2;
	}

	float maxChange = restDistance / 2;
	if (length(result) > maxChange)
	{
		result *= maxChange / length(result);
	}

	d_posOut[INDEX(gid.x, gid.y)] = tile[tid.y][tid.x] + result;
}


///////////////////////////////////////////////////////////////////////////////
// Input data:
// width and height - the dimensions of the particle grid
// d_pos            - the input positions
// spherePos        - The position of the sphere (xyz)
// sphereRad        - The radius of the sphere
//
// Output data:
// d_pos            - The updated positions
///////////////////////////////////////////////////////////////////////////////
__kernel void CheckCollisions(unsigned int width,
								unsigned int height, 
								__global float4* d_pos,
								float4 spherePos,
								float sphereRad){
	// Make sure the work-item does not map outside the cloth
	if (get_global_id(0) >= width || get_global_id(1) >= height)
		return;

	unsigned int particleID = get_global_id(0) + get_global_id(1) * width;
	float4 pos = d_pos[particleID];
	float4 d = pos - spherePos;
	float dist = d.x * d.x + d.y * d.y + d.z * d.z;

	// Find whether the particle is inside the sphere.
	if (dist < sphereRad * sphereRad)
	{
		// If so, push it outside.
		float4 posNew = pos + normalize(d) * (sphereRad - sqrt(dist));
		d_pos[particleID] = posNew;
	}
}

///////////////////////////////////////////////////////////////////////////////
// There is no need to change this function!
///////////////////////////////////////////////////////////////////////////////
float4 CalcTriangleNormal( float4 p1, float4 p2, float4 p3) {
    float4 v1 = p2-p1;
    float4 v2 = p3-p1;

    return cross( v1, v2);
}

///////////////////////////////////////////////////////////////////////////////
// There is no need to change this kernel!
///////////////////////////////////////////////////////////////////////////////
__kernel void ComputeNormals(unsigned int width,
								unsigned int height, 
								__global float4* d_pos,
								__global float4* d_normal){
								
    int particleID = get_global_id(0) + get_global_id(1) * width;
    float4 normal = (float4)( 0.0f, 0.0f, 0.0f, 0.0f);
    
    int minX, maxX, minY, maxY, cntX, cntY;
    minX = max( (int)(0), (int)(get_global_id(0)-1));
    maxX = min( (int)(width-1), (int)(get_global_id(0)+1));
    minY = max( (int)(0), (int)(get_global_id(1)-1));
    maxY = min( (int)(height-1), (int)(get_global_id(1)+1));
    
    for( cntX = minX; cntX < maxX; ++cntX) {
        for( cntY = minY; cntY < maxY; ++cntY) {
            normal += normalize( CalcTriangleNormal(
                d_pos[(cntX+1)+width*(cntY)],
                d_pos[(cntX)+width*(cntY)],
                d_pos[(cntX)+width*(cntY+1)]));
            normal += normalize( CalcTriangleNormal(
                d_pos[(cntX+1)+width*(cntY+1)],
                d_pos[(cntX+1)+width*(cntY)],
                d_pos[(cntX)+width*(cntY+1)]));
        }
    }
    d_normal[particleID] = normalize( normal);
}
